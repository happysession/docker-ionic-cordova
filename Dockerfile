#simple ionic-cli + cordova docker installation
#docker build -t hs/ionic-cli-cordova .
#or specify ionic-cli and cordova versions
#docker build --build-arg IONIC_CLI_VERSION=6.11.12 CORDOVA_VERSION=10.0.0

#FROM node:alpine

#alternative to reduce size instead of alpine, but does not
#include build tools for native compilation of npm packages
#we therefore add gcc
FROM node:lts-slim

LABEL maintainer="happy@hapysession.org" name="Ionic Cordova Builder" description="Ionic Cordova builder is a Docker image to build projects with Ionic + Cordova"

ARG USER_HOME_DIR="/tmp"
ARG APP_DIR="/app"
ARG USER_ID=1000

#reduce logging
ENV NPM_CONFIG_LOGLEVEL=warn 

#ionic-cli rc0 crashes with .ionic-cli.json in user home
ENV HOME "$USER_HOME_DIR"

#not declared to avoid anonymous volume leak
#but when not manually bound to host fs, performance will suffer!
#VOLUME "$USER_HOME_DIR/.cache/yarn"
#VOLUME "$APP_DIR/"
WORKDIR $APP_DIR
EXPOSE 4200

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

# ------------------------------------------------------
# --- JDK and UTILS
# ------------------------------------------------------

# avoid error processing package openjdk-8-jre:amd64
RUN mkdir -p /usr/share/man/man1

RUN apt-get update && apt-get install -qqy --no-install-recommends \
    openjdk-8-jdk \
    ca-certificates \
    dumb-init \
    git \
    build-essential \
    python \
    procps \
    rsync \
    curl \
    wget \
    unzip \
    openssh-client \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# ------------------------------------------------------
# --- Download Android Command line Tools into $ANDROID_SDK_ROOT
# ------------------------------------------------------
ARG ANDROID_COMMAND_LINE_TOOLS=6609375
ARG ANDROID_SDK_PLATFORM_TOOLS=29
ARG ANDROID_BUILD_TOOLS=29.0.3

# debian
ENV ANDROID_SDK_ROOT /opt/android-sdk-linux

RUN cd /opt \
    && wget -q https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_COMMAND_LINE_TOOLS}_latest.zip -O android-commandline-tools.zip \
    && mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools \
    && unzip -q android-commandline-tools.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools \
    && rm android-commandline-tools.zip 

RUN yes | ${ANDROID_SDK_ROOT}/cmdline-tools//tools/bin/sdkmanager --licenses \
    && ${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin/sdkmanager --no_https "build-tools;${ANDROID_BUILD_TOOLS}" \
    && ${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin/sdkmanager --no_https "platforms;android-${ANDROID_SDK_PLATFORM_TOOLS}"

ENV PATH=${PATH}:${ANDROID_SDK_ROOT}/build-tools:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin
# ------------------------------------------------------
# --- Install Gradle
# ------------------------------------------------------

# TODO : cette étape est nécéssaire cependant le test d'existance du chemin Cordova est réalisé uniquement sous Windows
# => sous Linux, l'installation de Gradle est systématique magré l'initialisation Docker.
# see myApp/platforms/android/cordova/lib/check_reqs.js

ARG GRADLE_VERSION=6.5
ENV GRADLE_HOME=/opt/gradle/gradle-${GRADLE_VERSION}/bin/
RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -P /tmp \
    && unzip -d /opt/gradle /tmp/gradle-*.zip \
    && chmod +775 /opt/gradle \
    && rm -rf /tmp/gradle*
ENV PATH=${PATH}:${GRADLE_HOME}

# ------------------------------------------------------
# --- IONIC + CORDOVA
# ------------------------------------------------------
ARG IONIC_CLI_VERSION=6.12.0
# change defaut IONIC_CONFIG_DIRECTORY (/tmp/.ionic) to avoir issue __os.userInfo__
# see https://github.com/ionic-team/ionic-cli/issues/4438
ARG NG_CLI_VERSION=10.1.7
ARG IONIC_CONFIG_DIRECTORY=$USER_HOME_DIR
ARG CORDOVA_VERSION=10.0.0
LABEL angular-cli=NG_CLI_VERSION ionic-cli=$IONIC_CLI_VERSION node=$NODE_VERSION

# npm 5 uses different userid when installing packages, as workaround su to node when installing
# see https://github.com/npm/npm/issues/16766
RUN set -xe \
    && mkdir -p $USER_HOME_DIR \
    && chown $USER_ID $USER_HOME_DIR \
    && chmod a+rw $USER_HOME_DIR \
    && mkdir -p $APP_DIR \
    && chown $USER_ID $APP_DIR \
    && chown -R node /usr/local/lib /usr/local/include /usr/local/share /usr/local/bin \
    && (cd "$USER_HOME_DIR"; su node -c "npm install -g @angular/cli@$NG_CLI_VERSION @ionic/cli@$IONIC_CLI_VERSION cordova@$CORDOVA_VERSION native-run; npm cache clean --force") 

USER $USER_ID