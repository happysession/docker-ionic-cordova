# docker-ionic-cli-cordova

Docker image for Ionic CLI (with Cordova) to use as build container.

Image on dockerhub: (happysession/ionic-cli-cordova)[https://hub.docker.com/repository/docker/happysession/ionic-cli-cordova/general]

Currently this image uses node 12 (npm 6) and node:lts-slim as base distribution.

```
$ docker build -t happysession/ionic-cli-cordova .
```

# Example app

```
$ docker run --rm -v "$PWD":/app hs/ionic-cli-cordova ionic start myApp tabs --cordova --type=angular --no-interactive --no-confirm
$ cd myApp
$ docker run  --rm -v "$PWD":/app hs/ionic-cli-cordova ionic info
Ionic:

   Ionic CLI                     : 6.11.12 (/usr/local/lib/node_modules/@ionic/cli)
   Ionic Framework               : @ionic/angular 5.3.5
   @angular-devkit/build-angular : 0.1000.8
   @angular-devkit/schematics    : 10.0.8
   @angular/cli                  : 10.0.8
   @ionic/angular-toolkit        : 2.3.3

Utility:

   cordova-res : not installed
   native-run  : not installed

System:

   NodeJS : v12.19.0 (/usr/local/bin/node)
   npm    : 6.14.8
   OS     : Linux 4.19
```

## Browser (PWA)

### Init PWA support
```
// Add PWA support
$ docker run --rm -v "$PWD":/app hs/ionic-cli-cordova npm run ng add @angular/pwa
```

### Build

```
// Build debug
$ docker run --rm -v "$PWD":/app hs/ionic-cli-cordova ionic cordova build browser --no-interactive --confirm

// Build prod
$ docker run --rm -v "$PWD":/app hs/ionic-cli-cordova ionic cordova build browser --pord --no-interactive --confirm
```

### Run

To run the Ionic CLI development server from docker you need to map the port and instruct Ionic CLI to listen on all interfaces.
For example use

```
// develop mode
$ docker run --rm -p 8100:8100 -v "$PWD":/app hs/ionic-cli-cordova ionic cordova run browser --livereload --host 0.0.0.0

// server mode TODO
$ docker run --rm -p 8100:8100 -v "$PWD":/app http-server -p 8080 -c-1  www/
```

## Android 

```
// debug
docker run --rm -v "$PWD":/app hs/ionic-cli-cordova cordova run android

//prod 
docker run --rm -v "$PWD":/app hs/ionic-cli-cordova cordova run android --prod
```

## iOS

**TODO**

# Analysers

**TODO**

# Auto DevOps

**TODO**